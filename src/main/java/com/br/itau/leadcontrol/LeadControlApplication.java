package com.br.itau.leadcontrol;
	import org.springframework.boot.SpringApplication;
		import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LeadControlApplication {

	public static void main(String[] args) {
		SpringApplication.run(LeadControlApplication.class, args);
	}

}
