package com.br.itau.leadcontrol.controllers;

        import com.br.itau.leadcontrol.enums.TipoDeLead;
        import com.br.itau.leadcontrol.models.Lead;
        import com.br.itau.leadcontrol.services.LeadService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/lead")
public class LeadController {

    @Autowired
    private LeadService leadService;

    @GetMapping("/{indice}")
    public Lead buscarLead(@PathVariable Integer indice){
        Lead lead = leadService.buscarPorIndice(indice);
        return lead;
    }

    @PostMapping("/")
    public Lead IncluirLead(@RequestBody Lead lead){
        Lead leadLead = leadService.insertLead(lead);
       return leadLead;
    }

}

