package com.br.itau.leadcontrol.services;


        import com.br.itau.leadcontrol.enums.TipoDeLead;
        import com.br.itau.leadcontrol.models.Lead;
        import org.springframework.stereotype.Service;

        import java.util.ArrayList;
        import java.util.Arrays;
        import java.util.List;

@Service
public class LeadService {
    private List<Lead> leads = new ArrayList(Arrays.asList(
            new Lead("Vinicius", "xablau@gmail.com", TipoDeLead.QUENTE)));

    public Lead buscarPorIndice(int indice){
        Lead lead = leads.get(indice);
        return lead;
    }

    public Lead insertLead(Lead lead){
        //String resposta = "Dados Inseridos: (";
        //resposta = resposta + lead.getNome() + "), (" + lead.getEmail() + "), (" + lead.getTipoDeLead() + ")";
        leads.add(lead);
        return lead;
    }
}

